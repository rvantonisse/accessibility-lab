{% extends "modules/views/_single-blog-item/_single-blog-item.html" %}
<!-- our-better-way -->

{% block blog %}
<div class="our-better-way" data-component="our-better-way">
	<div class="blogpost-illustrated-intro">
		<p>
			We as developers all know that the less time we lose on repetitive tasks, the better.
			It's no wonder open source projects like <a href="http://gruntjs.com/">Grunt</a>,
			<a href="http://gulpjs.com/">Gulp</a>, and more recently
			<a href="http://www.solitr.com/blog/2014/02/broccoli-first-release/">Broccoli</a>, have
			become more and more popular.
		</p>
		<figure>
			<img src="media/grunt-logo.min.png" alt="GruntJS">
		</figure>
	</div>

	<p>
		Developers want to focus on challenges and experimenting with new things, not on the
		<em>batch work</em>.
	</p>

	<p>
		At De Voorhoede we are no different from the common developers. We want more intelligent and
		clever ways to deliver our work. And Grunt has helped us achieve that.
	</p>

	<p>
		We've been working <em>cleverly</em> for some time now. But when we did it, it was on
		projects we started from scratch. Everything was thought of and structured as
		<a href="http://www.slideshare.net/jbmoelker/voorhoede-frontend-architecture/">components</a>,
		as we find that to be the best approach to modern web development.
	</p>

	<p>
		We can't always start a project from the beginning, and sometimes we have to maintain older
		projects. And that was the case for the project we'll be discussing here.
	</p>

	<p>
		This project is a project that our team have been working on for sometime, with quite a high
		developer rotation (including other teams). We had different developing phases,
		stopped for a few months, changed from SVN to Git, had no automation tasks and some other
		minor setbacks.
	</p>

	<p>
		These last sprints we had a bit of a delay from the backend party so we decided that
		it would be a great opportunity to improve our workflow.
	</p>

	<p>
		<em>(keep in mind that this was an intranet-kinda project)</em>
	</p>

	<h2>Project folder structure</h2>

	<p>
		Our initial project folder structure resembled something like this:
	</p>




<pre class="code-block language-markup"><code>■ some_party_once_involved/
	■ data/
	■ docs/
	■ src/
		■ fonts/
		■ images/
		■ scripts/ (a lot of javascript files)
		■ styles/ (a few less files)
		index.html
</code></pre>

	<p>
		It was a solid and commonly used file structure, but we were editing everything directly on
		the final files and were dependent on MAMP for the server. And worst of all, there was no
		automation. Each time the team wanted to push a new version to the set-top box (our final
		testing device), we had to:
	</p>
	<ol>
		<li>Push the changes to the repository</li>
		<li>Ask for the responsible developer to merge all the changes</li>
		<li>He would then connect to the VPN and then upload the files via FTP</li>
	</ol>
	<p>
		This was clearly not productive. We could do the local testing but not the final
		test environment as fast as we wanted.
	</p>
	<p>
		We called for our friend <a href="http://gruntjs.com/">Grunt</a>.
	</p>
	<h2>New folder structure</h2>
	<p>
		With the introduction of Grunt on our project we changed our structure a bit:
	</p>

<pre class="code-block language-markup">
<code>■ _root*
	■ data/
	■ docs/ (jsDoc generated documentation)
	■ grunt/
	■ node_modules/
	■ src/
		■ fonts/
		■ images/
		■ scripts/
		■ styles/
	■ web/
		■ src/
			■ fonts/ (symlink to fonts on parent directory)
			■ images/ (symlink to images on parent directory)
			■ scripts/ (concat+uglify single javascript file)
			■ styles/ (single minified css file)
		index.html</code>
</pre>

	<p>
		Configuring the node module <a href="https://www.npmjs.org/package/connect">&lsquo;connect&rsquo;</a> to <code>/web</code>
		we were ready to get to work in no time.
	</p>
	<h2>Inside the magic of grunt/</h2>
	<p>This is what our grunt folder looks like:</p>

<pre class="code-block language-markup">
<code>■ grunt/
	■ configuration/
		jshint.js
		sftp.js
		index.js
		...
	■ tasks/
		deploy.js
		develop.js
		test.js
</code>
</pre>

	<h3>configuration/</h3>
	<p>
		On the configuration directory we had all individual node module configurations.
		Example for <code>less.js</code>:
	</p>

<pre class="code-block language-javascript">
<code>function getConfiguration(grunt) {
	'use strict';
	return {
		development: {
			files: {
				"web/src/styles/style.css": "src/styles/style.less"
			}
		},
		distribution: {
			options: {
				cleancss: true
			},
			files: {
					"web/src/styles/style.css": "src/styles/style.less"
			}
		}
	};
}
module.exports = getConfiguration;</code>
</pre>

	<p>
		We would then load all the configurations for the node modules on our <code>index.js</code> file:
	</p>

<pre class="code-block language-javascript">
<code>function getConfiguration(grunt, taskList) {
	'use strict';
	var configuration = {
		pkg: grunt.file.readJSON('./package.json')
	};
	var length = taskList.length;
	var task;

	while (length--) {
		task = taskList[length];
		configuration[task] = require('./' + task)(grunt);
	}

	return configuration;
}
module.exports = getConfiguration;</code>
</pre>

	<h3>tasks/</h3>
	<p>
		On the tasks directory we had all the main tasks for developers to use.
	</p>
	<p>
		Example for <code>test.js</code>:
	</p>

<pre class="code-block language-javascript">
<code>module.exports = function (grunt) {
	'use strict';
	grunt.registerTask(
		'test',
		'Concatenates and minifies source files and sends it to the set-top box.',
		function () {
			grunt.task.run([
				'concat:distribution',
				'uglify',
				'copy:distribution',
				'less:distribution',
				'compress',
				'sftp'
			]);
		}
	);
};</code>
</pre>

	<p>Did you notice the <code>sftp</code> task?</p>
	<p>Exactly! Running this task, everybody on the team was able to quickly deploy the whole project to our testing device.</p>
	<h2>So much automation</h2>
	<p>Apart from the obvious benefits from deployment related tasks, we also improved:</p>
	<ol>
		<li>Live reload of Javascript / CSS</li>
		<li>Working with concatenated javascript + sourcemaps (closer to the real scenario)</li>
		<li>Separation of working directory and preview stage</li>
		<li>Better code on the fly with JSHint and CSSHint</li>
		<li>Happier faces :)</li>
	</ol>
	<p>The introduction of automation tasks is always a good idea.
		Don't let the idea of 'old projects must stay how the are so they don't break', prevent you from improving it.
	</p>
	<p>
		Always improve your workflow even if it's only for minifying a simple CSS file.
		The gains in the long run are always positive.
	</p>
	<h2>Looking to the future</h2>
	<p>
		Grunt is really good for what it was made for, single tasks!
		But its not ideal for development.
	</p>
	<blockquote>
		<p>
			Let’s say you’re using Grunt to build an application written with CoffeeScript, Sass, and a
			few more such compilers. As you develop, you want to edit files and reload the browser,
			without having to manually rebuild each time. So you use grunt watch, to rebuild
			automatically. But as your application grows, the build gets slower. Within a few months of
			development time, your edit-reload cycle has turned into an edit-wait-10-seconds-reload cycle.
			<a href="http://www.solitr.com/blog/2014/02/broccoli-first-release/">source</a>
		</p>
	</blockquote>
	<p>
		So in the future we are looking to move the development tasks to
		<a href="http://gulpjs.com/">Gulp</a> or even <a href="http://www.solitr.com/blog/2014/02/broccoli-first-release/">Broccoli</a>.
		They are both new (especially broccoli) so we are looking forward to see what improvements they
		can bring us.
	</p>
	<h4>note on symlink</h4>
	<div class="notes">
		<p>
			After some investigation on symlink of folders on windows systems, we concluded
			that we needed to create a separate task, just for windows developers that we had on the team.
		</p>

		<p>
			The task for them consisted in copying the symlink files at the beginning.
		</p>

		<p>
			The result turned out to be bad. Some had slow machines which turned every
			<code>grunt develop</code> task into a 10 second job. That was unacceptable.
		</p>

		<p>
			We later found out that Grunt already had the solution to that:
			<a href="https://www.npmjs.org/package/grunt-contrib-symlink">grunt-contrib-symlink</a>.
		</p>
	</div>
</div>
{% endblock %}

<!-- /our-better-way -->
