{% extends "modules/views/_single-blog-item/_single-blog-item.html" %}
<!-- presentation-oriented-templating -->

{% block blog %}
<div class="presentation-oriented-templating" data-component="presentation-oriented-templating">
	<p>
		As a web developer you have probably come across <strong>Presentation-Oriented
		Templating Syntax
		</strong>(<abbr title="Presentation-Priented Templating Syntax">POTS</abbr>) like this:
	</p>

{% raw %}
<pre class="code-block language-javascript">
<code>{% block content %}
	&lt;h1&gt;{{ section.title }}&lt;/h1&gt;

	{% for story in stories %}
	&lt;h2&gt;
		&lt;a href="{{ story.url }}"&gt;
			{{ story.title }}
		&lt;/a&gt;
	&lt;/h2&gt;
	&lt;p&gt;{{ story.teaser }}&lt;/p&gt;
	{% endfor %}
{% endblock %}</code></pre>
{%endraw%}

	<p>
		Without any prior experience of using templating engines, I’m pretty sure it was obvious
		how it worked. At least you knew what result to expect.
	</p>

	<p>
		This is by design: <em>the template system is meant to express presentation,
		not program logic</em>.
	</p>

	<p>
		Django, a Python framework, first introduced this templating syntax which was later
		adapted to work with other frameworks and rendering engines like Jinja (Python),
		Twig (PHP), H2o (PHP), Nunjucks (JavaScript) and Liquid (Ruby). While other different
		programming  languages and frameworks, the templating syntax for each is almost identical.
		Each supports the same notation for variables, filters, conditionals, inheritance,
		includes and comments.
	</p>

	<p>
		This kind of templating engine  provides tags which function similarly to some
		programming constructs –  if tag for boolean tests, a for tag for looping, etc.;
		template inheritance and includes – for managing layouts and components;
	</p>
	<p>
		In the following moments you will be able to get the key points of this kind of
		templating and also some small differences between the different languages that use them.
	</p>
	<h2>
		Why <abbr title="Presentation-Priented Templating Syntax">POTS</abbr> is great:
	</h2>
	<ul>
		<li>it has the same semantic and expressive syntax (notation) to bind data to markup for
			all frameworks using it</li>
		<li>can be used interchangeably between different frameworks and rendering engines</li>
		<li>has advanced methods to extend and include other templates</li>
		<li>is easy to read and write, both for developers and designers</li>
	</ul>

	<p>
		To the advantages pointed before, the best that we could add is that: if you know one,
		you really know how to work with all of them. Right ?
	</p>
	<p>
		Well, while they are pretty much the same, each one has some particular functionality
		that is usually related to the framework/language they are build on.
	</p>

	<p>
		So what are those little details and why should we care about it?
	</p>

	<p>
		First of all, because the syntax is similar on different back-ends, we can be sure that
		we can use it no matter which backend language is used.
	</p>

	<p>
		Secondly, even though they  all look pretty much the same, the project can benefit
		from a specific variation of the syntax.
	</p>

	<p>
		To understand the differences between the variations of templating frameworks using
		<abbr title="Presentation-Priented Templating Syntax">POTS</abbr>,
		we will be focusing on variables, filters, tags/conditionals and template inheritance.
	</p>

	<h2>Syntax key points</h2>
	<p>
		We will be comparing four variations of templating frameworks using
		<abbr title="Presentation-Priented Templating Syntax">POTS</abbr>:
	</p>

	<ul>
		<li>Django ( the ‘original’ Python flavour)</li>
		<li>Twig ( the PHP version)</li>
		<li>Nunjucks ( the JavaScript version)</li>
		<li>Liquid ( the ‘Shopify’ Ruby version)</li>

	</ul>
	<p>
		Since we can use the same data structure, we will use the following as an example:
	</p>

	<pre class="code-block language-javascript">
<code>{
	"title": "Fishing experiences",
	"stories": [
		{
			"title": "In my own town",
			"teaser": "My journey from noob to fishing expert",
			"url": "/story/2",
		},
		{
			"title": "Meristics",
			"teaser": "Introduction to Meristics basics",
			"url": "/story/3",
		}
	]
}</code></pre>
	{%raw%}
	<pre class="code-block language-javascript">
<code>{% block content %}
	&lt;h1&gt;{{ section.title }}&lt;/h1&gt;

	{% for story in stories %}
		&lt;h2&gt;&lt;a href="{{ story.url }}"&gt;{{ story.title }}&lt;/a&gt;&lt;/h2&gt;
		&lt;p&gt;{{ story.teaser }}&lt;/p&gt;
	{% endfor %}
{% endblock %}</code></pre>
	{%endraw%}


	<p>And the output would be:</p>

<pre class="code-block language-javascript">
<code>&lt;h1&gt;Fishing experiences&lt;/h1&gt;

&lt;h2&gt;&lt;a href="/story/2"&gt; In my own town &lt;/a&gt;&lt;/h2&gt;
&lt;p&gt;My journey from noob to fishing expert&lt;/p&gt;

&lt;h2&gt;&lt;a href="/story/3"&gt; Meristics &lt;/a&gt;&lt;/h2&gt;
&lt;p&gt;Introduction to Meristics basics&lt;/p&gt;
</code></pre>

	<h3>
		Variables
	</h3>
	{% raw %}
<pre class="code-block language-javascript">
<code>{{ foo.bar }}</code></pre>{%endraw%}
	<p>
		The application passes variables to the templates. Variables may have attributes
		or elements on them you can access as well. What a variable looks like heavily depends
		on the application providing it.
	</p>
	<p>
		This is the variable lookup order behind the scenes:
	</p>
	foo.bar
	<ul>
		<li>check if there is an attribute called bar on foo.</li>
		<li>if there is not, check if there is an item 'bar' in foo.</li>
		<li>if there is not, return an undefined object.</li>
	</ul>

	foo['bar']
	<ul>
		<li>check if there is an item 'bar' in foo.</li>
		<li>if there is not, check if there is an attribute called bar on foo.</li>
		<li>if there is not, return an undefined object.</li>
	</ul>

	<strong>Twig</strong> does a slightly different approach:

	<ul>
		<li>check if foo is an array and bar a valid element;</li>
		<li>if not, and if foo is an object, check that bar is a valid property;</li>
		<li>if not, and if foo is an object, check that bar is a valid method (even if bar
		is the constructor - use __construct()instead);</li>
		<li>if not, and if foo is an object, check that getBar is a valid method;</li>
		<li>if not, and if foo is an object, check that isBar is a valid method;</li>
		<li>if not, return a null value.</li>
	</ul>

	foo['bar'] on the other hand only works with PHP arrays:
	<ul>
		<li>check if foo is an array and bar a valid element;</li>
		<li>if not, return a null value.</li>
	</ul>

	<p>
		It's important to know that the curly braces are not part of the variable, but of the
		print statement. If you access variables inside tags, don't put the braces around.
	</p>

	<p>
		If a value is undefined or null, nothing is displayed. The same behavior occurs
		when referencing undefined or null objects.
	</p>

	<p>
		You can also set variables inside the block scope.
	</p>

	<p>
		<strong>Twig</strong> flavored syntax adds extra functionalities like
		<a href="http://twig.sensiolabs.org/doc/templates.html#global-variables">global
		variables</a> to help the developer.
	</p>

	<h2>Filters:</h2>

	<p>
		You can modify variables for display by using filters.
	</p>
	<p>
		Filters look like this: {% raw %} {{ name | lower }} {% endraw %}. This displays the value
		of the  {% raw %}{{ name }} {% endraw %} variable after being filtered through the lower
		filter, which converts text to lowercase. Use a pipe (|) to apply a filter.
	</p>

	<p>
		Filters can be “chained.” The output of one filter is applied to the next.
		{% raw %}{{ text | escape | linebreaks }}{% endraw %} is a common idiom for escaping text
		contents, then converting line breaks to < p > tags.
	</p>

	<p>
		Some filters take arguments.  A filter argument looks like this:
		{% raw %}{{ bio | truncatewords:30 }}{% endraw %}. This will display the first 30
		words of the bio variable.
		Filter arguments that contain spaces must be quoted; for example, to join
		a list with commas and spaced you’d use  {% raw %}{{ list | join:", " }}{% endraw %}.
	</p>

	<p>
		All of the flavored syntaxes have a set of base filters like: capitalize,
		escape (sometimes shortened to just e),  join, last, lenght, lower, random,
		reverse, slice, sort and float.
	</p>
	<p>
		But some of them extend their base with:
	</p>
	<ul>
		<li><strong>Twig</strong> adds
			<a href="http://twig.sensiolabs.org/doc/filters/date_modify.html">date_modify</a>,
			<a href="http://twig.sensiolabs.org/doc/filters/json_encode.html">json_encode</a>,
			<a href="http://twig.sensiolabs.org/doc/filters/nl2br.html">nl2br</a>
		</li>
		<li><strong>Liquid</strong> adds
			<a href="https://github.com/Shopify/liquid/wiki/Liquid-for-Designers#standard-filters">
				truncatewords</a>
		</li>
	</ul>

	<p>
		You can always add your own custom filters.
	</p>

	<h2>Conditionals / Tags</h2>

	<p>Tags look like this: {% raw %}{% tag %}{% endraw %}. Tags are more complex than variables:
		Some create text in the output, some control flow by performing loops or logic, and some
		load external information into the template to be used by later variables.</p>

	<p>Some tags require beginning and ending tags (i.e. {% raw %}{% tag %}{% endraw %} ... tag
		contents ... {% raw %}{% endtag %}{% endraw %}).</p>

	<p>The most common include: if, for, extend, include, macros and comments.</p>

	<h3>If</h3>

	<p>
		if tests a condition and lets you selectively display content. It behaves exactly as
		javascript's if behaves.</p>

	{% raw %}

	<pre class="code-block language-javascript">
<code>{% if fishes %}
	We have some {{fishes}}
{% endif %}</code></pre>

	{% endraw %}

	<h3>for</h3>

	<p>for iterates over arrays and dictionaries.</p>
	{% raw %}

<pre class="code-block language-javascript">
<code>{% for item in items %}
	{{ item.title }}
{% endfor %}</code></pre>

	{% endraw %}


	<h3>Macros</h3>
	<p>
		Macros are comparable with functions in regular programming languages. They are useful
		to reuse often used HTML fragments to not repeat yourself.
	</p>

	{% raw %}

	<pre class="code-block language-javascript">
<code>{% macro input(name, value, type) %}
	&lt;input type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|escape }}"/&gt;
{% endmacro %}</code></pre>

	{% endraw %}

	<p>
		And then they could be used like:
	</p>

	{% raw %}
<pre class="code-block language-javascript"><code>&lt;dl>
	&lt;dt>Fish name&lt;/dt&gt;
	&lt;dd>{{ input_field(fish_name) }}&lt;/dd&gt;
	&lt;dt>Scientific name&lt;/dt&gt;
	&lt;dd>{{ input_field('scientific_name') }}&lt;/dd&gt;
&lt;/dl&gt;</code></pre>

	{% endraw %}
	<p>
		One special note about the {% raw %}{% raw %}{% endraw %} tag. If you
		are using a server-side template, to not parse that part of the code as a template.
	</p>

<pre class="code-block language-javascript"><code>{% raw %}{% raw %}
	{% if %} this will {{ not be processed }} {% endif %}
{ % endraw %}
{% endraw %}</code></pre>

	<h3>Comments</h3>

	<p>You can write comments using {# and #}. Comments are completely stripped out when rendering.
		Comments are nothing more than a Tag.</p>

	<pre class="code-block language-javascript"><code>{% raw %}{# disabled type of catched fished because of Marine Conservation Institute
	{% for fish in fishes %}
	...
	{% endfor %}
#}{% endraw %}</code></pre>

	<h3>Bonus from specific variations:</h3>
	<ul>
		<li>
			<strong>Nunjucks</strong> adds
			<a href="http://jlongster.github.io/nunjucks/templating.html#asynceach">asyncEach</a>
			and <a href="http://jlongster.github.io/nunjucks/templating.html#asyncall">asyncAll</a>
			for working with asynchronous javascript
		</li>
		<li>
			<strong>Liquid</strong> adds
			<a href="https://github.com/Shopify/liquid/wiki/Liquid-for-Designers#if--else">
			unless</a> for negation of if statements,
			and <a href="https://github.com/Shopify/liquid/wiki/Liquid-for-Designers#cycle">cycle
			</a> to alternate  between different tasks.
		</li>
	</ul>

	<h3>Template Inheritance</h3>
	<p>
		Template inheritance is a way to make it easy to reuse templates. When writing a
		template, you can define "blocks" that child templates can override <br/>
		You start by defining a ‘base’ template:
	</p>

	{% raw %}

	<pre class="code-block language-javascript"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
	&lt;head&gt;
	{% block head %}
		&lt;link rel="stylesheet" href="style.css" /&gt;
		&lt;title>{% block title %}{% endblock %}&lt;/title&gt;
	{% endblock %}
	&lt;/head&gt;
	&lt;body&gt;
		&lt;div id="content"&gt;{% block content %}{% endblock %}&lt;/div&gt;
	&lt;/body&gt;
&lt;/html&gt;</code></pre>

	{% endraw %}

	<p>And then you extend the base layout, by filling the blocks with content:</p>

	{% raw %}

	<pre class="code-block language-javascript"><code>{% extends "base.html" %}
{% block title %}Index{% endblock %}
{% block head %}
{% endblock %}
{% block content %}
	&lt;h1&gt;Index&lt;/h1&gt;
	&lt;p class="important">
		Welcome to Fishing experiences.
	&lt;/p&gt;
{% endblock %}</code></pre>

	{% endraw %}

	<h2>To wrap up</h2>
	<p>
		With variables and tags you can simply cycle through any content. Filters let you
		customize the output given from the data source. Macros help you don’t repeat yourself
		and with templating inheritance you organize your templates in a structured way.
	</p>

	<p>
		If you want to know more you can check this references:
	</p>

	<ul>
		<li>
			<a href="http://en.wikipedia.org/wiki/Template_engine_(web)">
				http://en.wikipedia.org/wiki/Template_engine_(web)
			</a>
		</li>
		<li>
			<a href="http://twig.sensiolabs.org/doc/templates.html">
				http://twig.sensiolabs.org/doc/templates.html
			</a>
		</li>
		<li>
			<a href="http://jlongster.github.io/nunjucks/templating.html">
				http://jlongster.github.io/nunjucks/templating.html
			</a>
		</li>
		<li>
			<a href="https://docs.djangoproject.com/en/1.6/topics/templates/">
				https://docs.djangoproject.com/en/1.6/topics/templates/
			</a>
		</li>
		<li>
			<a href="https://github.com/Shopify/liquid/wiki/Liquid-for-Designers">
				https://github.com/Shopify/liquid/wiki/Liquid-for-Designers
			</a>
		</li>
	</ul>

	<p>
		Some alternatives:
	</p>
	<ul>
		<li>
			<a href="https://github.com/speedmax/h2o-php">
				H20
			</a>
		</li>
		<li>
			<a href="http://jinja.pocoo.org/">
				Jinja
			</a>
		</li>

		<li>
			<a href="http://paularmstrong.github.io/swig/">
				Swig
			</a>
		</li>
	</ul>
</div>
{% endblock %}

<!-- place the scripts in a /js folder of this item -->
{% block scripts %}
{{ super() }}
{% endblock %}

<!-- /presentation-oriented-templating -->
