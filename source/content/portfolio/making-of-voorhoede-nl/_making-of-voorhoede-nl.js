(function() {
	'use strict';

	/**
	 * Modern Element selector support
	 * @type {Boolean}
	 */
	var hasQuerySelectorSupport = ('querySelector' in document && 'querySelectorAll' in document);

	/**
	 * DOM classList support
	 * @type {Boolean}
	 */
	var hasClassListSupport = ('classList' in document.documentElement);

	/**
	 * Check if it's the 'making-of-voorhoede-nl' page
	 * @type {Boolean}
	 */
	var isMakingOfPage = (document.location.pathname === '/portfolio/making-of-voorhoede-nl/');

	if(hasQuerySelectorSupport && hasClassListSupport && isMakingOfPage){
		initInteractiveMakingOf();
	}

	function initInteractiveMakingOf(){
		var activeStepNumber;
		var buttonElements = document.querySelectorAll('[data-step]');

		document.body.classList.add('making-of-interactive');

		function toggleStep(event){
			var buttonElement = event.target;
			var stepNumber = parseInt(buttonElement.getAttribute('data-step'), 10);
			var isActive = (stepNumber === activeStepNumber);
			document.body.classList.remove('making-of-step-' + activeStepNumber);
			if(isActive){
				activeStepNumber -= 1;
			} else {
				activeStepNumber = stepNumber;
			}
			document.body.classList.add('making-of-step-' + activeStepNumber);
		}

		// bind click handlers to all step button elements
		for(var index = 0, length = buttonElements.length; index < length; index++){
			var buttonElement = buttonElements[index];
			buttonElement.addEventListener('click', toggleStep);
		}
	}

}());