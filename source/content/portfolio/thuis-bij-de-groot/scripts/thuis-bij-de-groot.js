(function($) {
	'use strict';

	// autoplay for videos
	// extend this as needed
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf('android') > -1;

	var IS_IOS_DEVICE = (function () {
		var platform = /\b(?:ip(hone|(a|o)d))\b/i.test(window.navigator.platform);
		var vendor = /\b(?:apple)\b/i.test(window.navigator.vendor);
		return platform && vendor;
	}());
	var DO_AUTOPLAY_VIDEO = !IS_IOS_DEVICE && !isAndroid;

	if (DO_AUTOPLAY_VIDEO) {
		$('video').each(function (index, video) {
			if (video.hasAttribute('data-autoplay')) {
				video.controls = false;
				video.preload = 'auto';
				video.autoplay = true;
			}
		});
	}

	//Slider on 'Cases' page ('Uitgelicht')
	//initialize only if its elements exist on page

	var slider = $('.m-scooch');
	if(slider.length){
		slider = slider.scooch();
		var disabledClass = 'disabled';
		var nextControlClass = '.next-control';
		var prevControlClass = '.prev-control';

		slider.on('afterSlide', function(e, previousSlide, nextSlide) {

			var $target = $(e.target);
			var totalSlides = $target.find('.m-scooch-inner').children().length;

			$target.find(nextControlClass).removeClass(disabledClass);
			$target.find(prevControlClass).removeClass(disabledClass);

			if (nextSlide === totalSlides) {
				$target.find(nextControlClass).addClass(disabledClass);
			}
			else if (nextSlide === 1) {
				$target.find(prevControlClass).addClass(disabledClass);
			}
		});
	}

})(('undefined' === typeof Zepto) ? jQuery : Zepto);