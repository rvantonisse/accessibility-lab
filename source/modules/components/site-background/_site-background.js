helpers = helpers || {};

(function() {
    'use strict';

    var planesHeight, totalPlanesHeight;
	var BASE_FONT_HEIGHT = 17;

    function placeBackground() {

        // get the dimensions of the page
        var docHeight = (document.height !== undefined) ? document.height : document.body.offsetHeight,
            bgPlanes = document.querySelector('.bg-planes').getElementsByTagName('div'),
            lastBgPlane = bgPlanes[bgPlanes.length - 1];

        // ie get style attribute
        if (lastBgPlane.currentStyle) {
            planesHeight = Math.round(parseFloat(lastBgPlane.currentStyle['top'].replace('em', '')) * BASE_FONT_HEIGHT);
        } else if (window.getComputedStyle) {
            planesHeight = parseInt(document.defaultView.getComputedStyle(lastBgPlane, null).getPropertyValue('top').replace('px', ''), 10);
        }

        // the height of one individual group of planes multiplied by the number of groups
        totalPlanesHeight = document.querySelectorAll('.bg-planes').length * planesHeight;

        // if background is large enough do not duplicate and return
        if (docHeight > totalPlanesHeight) {
			var copyCount = Math.floor(docHeight/totalPlanesHeight);

			for (var i = 1; i < copyCount + 1; i++) {
				// copy background if document height is greater than initial height of the background
				var copy = document.querySelector('.bg-planes').cloneNode(true);
				// append copied html and set top to end of background
				document.querySelector('.page-background').appendChild(copy).style.top = totalPlanesHeight * i + 'px';
			}
        }
    }

    placeBackground();
    // on resize redraw background
    window.onresize = placeBackground;

	helpers.placeBackground = placeBackground;
})();

