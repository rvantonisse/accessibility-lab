(function($){
	'use strict';

	var $window = $(window);
	var $html = $('html');
	var $page = $('#page');
	var $main = $('#main');
	var $siteMenu = $('.js-site-menu');
	var $siteLogo = $('.header-wrapper a');
	var $pageHeader = $('.site-header');
	var $pageSection = $('.page-section');
	var $overlapElement = $main.children().eq(0);
	var overlapOffset;
	var lastScrollTop = 0;
	var scrollPosition = $window.scrollTop();
	var visitedSections = [];


	/**
	 * Change header appearance and visibility on scroll.
	 * The scroll dummy works around a webkit bug in iOS < 6 and latest Chrome.
	 */

	var $scrollDummy;
	$scrollDummy = $('<div/>');
	$scrollDummy.css('height', 0);
	$main.append($scrollDummy);


	// highlight current element in navigation
	var isActivated = false;

	$siteMenu.on('click', 'a', function () {
		var tokens = this.href.split('#');
		var sectionId = tokens[1];

		if (sectionId) {
			setActiveMenuItem(sectionId);
			isActivated = true;
		}

		// analytics section view trigger
		if (visitedSections.indexOf(sectionId) === -1) {
			sendAnalyticsViewEvent(sectionId);
			visitedSections.push(sectionId);
		}


	});

	$siteLogo.on('click', function() {
		$('.js-nav-title a[href]')
			.removeClass('active');
	});

	function setActiveMenuItem(id) {
		$('.js-nav-title a[href]')
			.removeClass('active')
			.filter('[href$="' + id + '"]')
			.addClass('active');
	}

	function setScrollSpy() {
		$window.scroll(function () {
			if (isActivated) {
				isActivated = false;
				return;
			}
			$pageSection.each(function () {
				var element = $(this);
				var viewportTop = $(window).scrollTop(); // vertical scroll position
				var sectionTop = element.offset().top - 350; // vertical position of element
				var sectionBottom = sectionTop + element.height(); // vertical position of element + height of element

				if (viewportTop >= sectionTop && viewportTop <= sectionBottom) {
					var sectionId = element.attr('id');
					setActiveMenuItem(sectionId);

					// analytics section view trigger
					if (visitedSections.indexOf(sectionId) === -1) {
						sendAnalyticsViewEvent(sectionId);
						visitedSections.push(sectionId);
					}
				}
			});
		});
	}

	function setOverlap() {
		overlapOffset = ($overlapElement[0].offsetTop - $pageHeader[0].offsetHeight);
	}

	function supportsSVG() {
		return !! document.createElementNS && !! document.createElementNS('http://www.w3.org/2000/svg','svg').createSVGRect;
	}

	$window.on('load', function () {
		var locationTokens = window.location.href.split(['#','/']);
		setOverlap();


		if (locationTokens[1]) {
			setActiveMenuItem(locationTokens[1]);
			window.setTimeout(setScrollSpy, 50);
		} else {
			setScrollSpy();
			//trigger the scroll handler to highlight on page load
			$window.scroll();
		}

		if (!supportsSVG()) {
			var imgs = document.getElementsByTagName('img');
			var dotSVG = /.*\.svg$/;
			for (var i = 0; i !== imgs.length; ++i) {
				if(imgs[i].src.match(dotSVG)) {
					imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
				}
			}
		}
	});

	$window.on('resize', setOverlap);


	if ($html.hasClass('lt-ie9')) {
		return;
	}

	// show collapsed menu on mobile
	var $collapsedMenu = $('<div class="js-collapsed-menu"></div>');
	$siteMenu.before($collapsedMenu);
	// NB: the simplest way to have generic click event delegation
	// on iOS is to use a wrapper around all content
	$page.on('click', function () {
		$collapsedMenu.removeClass('open');
		$siteMenu.removeClass('open');
	});
	$collapsedMenu.on('click', function (event) {
		$siteMenu.toggleClass('open');
		$pageHeader.toggleClass('header-on-menu-open');
		$collapsedMenu.toggleClass('open');
		event.stopPropagation();
	});

	// show/hide page header on scroll up/down

	function headerOnScroll() {
		// if(window.location.pathname == '/'){
		$window.on('scroll', function () {
			scrollPosition = $window.scrollTop();

			if (scrollPosition > 0) {

				// scroll down
				if (scrollPosition > lastScrollTop) {
					$pageHeader.css({
						'position'	:	'absolute'
					}).removeClass('site-header-on-scroll');

				}

				// scroll up
				if ((scrollPosition < lastScrollTop)) {
					$pageHeader.css({
						'position'	:	'fixed'
					}).addClass('site-header-on-scroll');
					if($siteMenu.height() > $(window).height()) {
						$collapsedMenu.on('click', function () {
							$window.scrollTop(0);
						});
					}
				}

			} else {
				$pageHeader.css({
					'position'	:	'absolute'
				}).removeClass('site-header-on-scroll');
			}
			lastScrollTop = scrollPosition;
		});
		//}
	}

	function sendAnalyticsViewEvent(name) {
		// requires analytics to be loaded!
        if (typeof ga !== 'undefined') {
            ga('send', {
                'hitType': 'event',
                'eventCategory': 'homepage',
                'eventAction': 'view',
                'eventLabel': name
            });
        }
	}

	headerOnScroll();


})(('undefined' === typeof Zepto) ? jQuery : Zepto);
